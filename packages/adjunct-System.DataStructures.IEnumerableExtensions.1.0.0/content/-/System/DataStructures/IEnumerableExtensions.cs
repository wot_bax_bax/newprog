﻿using System.Collections;
using System.Collections.Generic;

namespace System.DataStructures
{
	internal static class IEnumerableExtensions
	{
		public static bool IsNullOrEmpty<T>(this IEnumerable<T> items)
		{
			if (items == null)
			{
				return true;
			}
			ICollection<T> collectionOfT = items as ICollection<T>;
			if (collectionOfT != null)
			{
				return collectionOfT.Count == 0;
			}
			ICollection collection = items as ICollection;
			if (collection != null)
			{
				return collection.Count == 0;
			}
			using (IEnumerator<T> e = items.GetEnumerator())
			{
				return !e.MoveNext();
			}
		}

		public static bool IsNullOrEmpty(this IEnumerable items)
		{
			if (items == null)
			{
				return true;
			}
			ICollection collection = items as ICollection;
			if (collection != null)
			{
				return collection.Count == 0;
			}
			IEnumerator enumerator = items.GetEnumerator();
			bool isEmpty = !enumerator.MoveNext();
			IDisposable disposable = enumerator as IDisposable;
			if (disposable != null)
			{
				disposable.Dispose();
			}
			return isEmpty;
		}
	}
}