﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Proxies;
using System.Text;
using System.Threading.Tasks;
using ConfigurationApp;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;

namespace NewProg
{
    public class ProxySettings
    {
        public Boolean Enabled { get; set; }
        public int Host { get; set; }
        public int Port { get; set; }

        public class GeneralSettings : ProxySettings
        {
            public ProxySettings[] proxysettings;
            public class Proxy : ProxySettings
            {
                public string proxy;

                double[] doublesArray = new double[4];

            ProxySettings firstproxy = new ProxySettings();

            ProxySettings secondproxy = new ProxySettings();
                
            ProxySettings thirdproxy = new ProxySettings();
               
            ProxySettings fourthproxy = new ProxySettings();
                
            ProxySettings fifthproxy = new ProxySettings();
               
        }

        }
        class Program
        {

            static void Main(string[] args)
            {
                var GeneralSettings = new GeneralSettings();
                var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();
               
                var host = new WebHostBuilder()
                .UseKestrel()
                .UseUrls(hostUrl)
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseStartup<Startup>()
                .Build();

                host.Run();

            }

        }
    }

    namespace ConfigurationApp
    {
        public class ProxyСonfiguration
        {
            private readonly RequestDelegate _next;

            public ProxyСonfiguration(RequestDelegate next, IOptions<ProxySettings> options)
            {
                _next = next;
                ProxySettings = options.Value;
            }

            public ProxySettings ProxySettings { get; }

            public async Task InvokeAsync(HttpContext context)
            {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append($"<p>Enabled: {ProxySettings?.Enabled}</p>");
                stringBuilder.Append($"<p>Host: {ProxySettings?.Host}</p>");
                stringBuilder.Append($"<p>Port: {ProxySettings?.Port}</p>");

                await context.Response.WriteAsync(stringBuilder.ToString());
            }
        }
    }
}