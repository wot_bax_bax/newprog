﻿using System.Collections.Generic;

namespace System.DataStructures
{
	internal class TestingSet : SetBase<int>
	{
		private readonly List<int> _storage = new List<int>();

		public TestingSet(params int[] initialContents)
		{
			if (initialContents != null)
			{
				UnionWith(initialContents);
			}
		}

		public TestingSet(IEnumerable<int> initialContents = null)
		{
			if (initialContents != null)
			{
				UnionWith(initialContents);
			}
		}

		public override int Count
		{
			get { return _storage.Count; }
		}

		public override bool Add(int item)
		{
			if (_storage.Contains(item))
			{
				return false;
			}
			_storage.Add(item);
			return true;
		}

		public override void Clear()
		{
			_storage.Clear();
		}

		public override ISetEx<int> CloneType(IEnumerable<int> initialContents = null)
		{
			return new TestingSet(initialContents);
		}

		public override bool Contains(int item)
		{
			return _storage.Contains(item);
		}

		public override IEnumerator<int> GetEnumerator()
		{
			return _storage.GetEnumerator();
		}

		public override bool Remove(int item)
		{
			return _storage.Remove(item);
		}
	}
}