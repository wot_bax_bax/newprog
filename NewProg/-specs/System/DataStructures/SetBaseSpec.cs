﻿using Xunit;

namespace System.DataStructures
{
	public class SetBaseSpec
	{
		[Fact]
		public void CopyTo()
		{
			TestingSet setA = new TestingSet(1, 2, 3);
			int[] array = new int[3];
			setA.CopyTo(array, 0);
			Assert.Equal(1, array[0]);
			Assert.Equal(2, array[1]);
			Assert.Equal(3, array[2]);
		}

		[Fact]
		public void Difference()
		{
			TestingSet setA = new TestingSet(1, 2, 3);
			TestingSet setB = new TestingSet(2, 3, 4);
			ISetEx<int> difference = setA.Difference(setB);
			Assert.Equal(1, difference.Count);
			Assert.True(difference.SetEquals(new[] {1, 1}));
			Assert.Equal(setA, setA.Difference(null));
		}

		[Fact]
		public void GetSubset()
		{
			TestingSet setA = new TestingSet(1, 2, 3, 4, 5);
			ISetEx<int> subset = setA.GetSubset(i => i%2 == 0);
			Assert.Equal(2, subset.Count);
			Assert.True(subset.SetEquals(new[] {2, 4}));
		}

		[Fact]
		public void Intersection()
		{
			TestingSet setA = new TestingSet(1, 2, 3);
			TestingSet setB = new TestingSet(2, 3, 4);
			ISetEx<int> intersect = setA.Intersect(setB);
			Assert.Equal(2, intersect.Count);
			Assert.True(intersect.SetEquals(new[] {3, 2}));
			Assert.Equal(new TestingSet(), setA.Intersect(null));
		}

		[Fact]
		public void IsEmpty()
		{
			TestingSet set = new TestingSet();
			Assert.True(set.IsEmpty);
			set.Add(1);
			Assert.False(set.IsEmpty);
		}

		[Fact]
		public void Overlaps()
		{
			TestingSet setA = new TestingSet(1, 2, 3);
			TestingSet setB = new TestingSet(2, 3, 4);
			Assert.True(setA.Overlaps(setB));
		}

		[Fact]
		public void Subset()
		{
			TestingSet setA = new TestingSet(1, 2, 3);
			TestingSet setB = new TestingSet(2, 3);
			Assert.True(setA.IsSubsetOf(setA));
			Assert.True(setB.IsSubsetOf(setA));
			Assert.False(setA.IsProperSubsetOf(setA));
			Assert.True(setB.IsProperSubsetOf(setA));
		}

		[Fact]
		public void Superset()
		{
			TestingSet setA = new TestingSet(1, 2, 3);
			TestingSet setB = new TestingSet(2, 3);
			Assert.True(setA.IsSupersetOf(setA));
			Assert.True(setA.IsSupersetOf(setB));
			Assert.False(setA.IsProperSupersetOf(setA));
			Assert.True(setA.IsProperSupersetOf(setB));
		}

		[Fact]
		public void SymmetricDifference()
		{
			TestingSet setA = new TestingSet(1, 2, 3);
			TestingSet setB = new TestingSet(2, 3, 4);
			ISetEx<int> symDifference = setA.SymmetricDifference(setB);
			Assert.Equal(2, symDifference.Count);
			Assert.True(symDifference.SetEquals(new[] {1, 4}));
			Assert.Equal(setA, setA.SymmetricDifference(null));
		}

		[Fact]
		public void Union()
		{
			TestingSet setA = new TestingSet(1, 2, 3);
			TestingSet setB = new TestingSet(2, 3, 4);
			ISetEx<int> union = setA.Union(setB);
			Assert.Equal(4, union.Count);
			Assert.True(union.SetEquals(new[] {4, 3, 2, 1}));
			Assert.Equal(setA, setA.Union(null));
		}
	}
}